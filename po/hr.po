# Croatian translation for metronome.
# Copyright (C) 2022 metronome's COPYRIGHT HOLDER
# This file is distributed under the same license as the metronome package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: metronome master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/metronome/-/issues\n"
"POT-Creation-Date: 2022-03-09 10:17+0000\n"
"PO-Revision-Date: 2022-03-11 20:58+0100\n"
"Last-Translator: \n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.0.1\n"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Općenito"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Prikaži prečace"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zatvori"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Tap"
msgstr "Dodirni"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "_Prečaci tipkovnice"

#: data/resources/ui/window.ui:9
msgid "_About Metronome"
msgstr "_O Metronomu"

#: data/resources/ui/window.ui:58
msgid ""
"2\n"
"4"
msgstr ""
"2\n"
"4"

#: data/resources/ui/window.ui:77
msgid ""
"3\n"
"4"
msgstr ""
"3\n"
"4"

#: data/resources/ui/window.ui:96
msgid ""
"4\n"
"4"
msgstr ""
"4\n"
"4"

#: data/resources/ui/window.ui:115
msgid ""
"6\n"
"8"
msgstr ""
"6\n"
"8"

#: data/resources/ui/window.ui:207
msgid "TAP"
msgstr "DODIRNI"

#: data/com.adrienplazas.Metronome.desktop.in.in:3
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:7 src/main.rs:25
msgid "Metronome"
msgstr "Metronom"

#: data/com.adrienplazas.Metronome.desktop.in.in:4
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:8
msgid "Keep the tempo"
msgstr "Zadržite tempo"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.adrienplazas.Metronome.desktop.in.in:10
msgid "Bar;Beat;Beats;BPM;Measure;Minute;Rhythm;Tap;Tempo;"
msgstr "Takt;Otkucaj;Otkucaji;BPM;Mjera;Minuta;Ritam;Dodirni;Tempo;"

#: data/com.adrienplazas.Metronome.gschema.xml.in:6
#: data/com.adrienplazas.Metronome.gschema.xml.in:7
msgid "Default window width"
msgstr "Zadana širina prozora"

#: data/com.adrienplazas.Metronome.gschema.xml.in:11
#: data/com.adrienplazas.Metronome.gschema.xml.in:12
msgid "Default window height"
msgstr "Zadana visina prozora"

#: data/com.adrienplazas.Metronome.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "Zadano ponašanje uvećanja prozora"

#: data/com.adrienplazas.Metronome.gschema.xml.in:22
#: data/com.adrienplazas.Metronome.gschema.xml.in:23
msgid "Default beats per bar"
msgstr "Zadani broj otkucaja po taktu"

#: data/com.adrienplazas.Metronome.gschema.xml.in:28
#: data/com.adrienplazas.Metronome.gschema.xml.in:29
msgid "Default beats per minute"
msgstr "Zadani broj otkucaja u minuti"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:10
msgid ""
"Metronome beats the rhythm for you, you simply need to tell it the required "
"time signature and beats per minutes."
msgstr ""
"Metronom otkucava ritam, jednostavno mu zadajte željeni vremenski raspon i "
"broj otkucaja u minuti."

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:11
msgid ""
"You can also tap to let the application guess the required beats per minute."
msgstr "Možete dodirnuti Metronom kako bi pogodio potrebne otkucaje u minuti."

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:16
msgid "Main window"
msgstr "Glavni prozor"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:20
msgid "Main window, narrow and running"
msgstr "Glavni prozor, uzak i pokrenut"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:63
msgid "Adrien Plazas"
msgstr "Adrien Plazas"
