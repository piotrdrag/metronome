<?xml version="1.0" encoding="UTF-8"?>
<!-- Adrien Plazas 2019 <kekun.plazas@laposte.net> -->
<component type="desktop-application">
  <id>@app-id@</id>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <name>Metronome</name>
  <summary>Keep the tempo</summary>
  <description>
    <p>Metronome beats the rhythm for you, you simply need to tell it the required time signature and beats per minute.</p>
    <p>You can also tap to let the application guess the required beats per minute.</p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/metronome/raw/1.2.1/data/resources/screenshots/screenshot1.png</image>
      <caption>Main window</caption>
    </screenshot>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/metronome/raw/1.2.1/data/resources/screenshots/screenshot2.png</image>
      <caption>Main window, narrow and running</caption>
    </screenshot>
  </screenshots>
  <url type="homepage">https://gitlab.gnome.org/World/metronome</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/metronome/issues</url>
  <url type="donation">https://liberapay.com/adrienplazas</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/metronome</url>
  <url type="translate">https://l10n.gnome.org/module/metronome/</url>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="1.3.0" date="2023-06-14">
      <description>
        <p>The 1.3.0 release of Metronome brings bug fixes and general improvements.</p>
        <ul>
          <li>Added tooltips to various buttons</li>
          <li>Change the BPM without pausing</li>
          <li>Fixed a bug with ticks being off-beat</li>
        </ul>
      </description>
    </release>
    <release version="1.2.1" date="2023-05-11">
      <description>
        <p>The 1.2.1 release of Metronome brings bug fixes.</p>
        <ul>
          <li>Update the application to GNOME 44.</li>
          <li>UI improvements.</li>
          <li>Control sound via spacebar.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.0" date="2021-04-13">
      <description>
        <p>The 1.2.0 release of Metronome brings bug fixes.</p>
        <ul>
          <li>Update the application to GNOME 42.</li>
          <li>Keep the BPM setting between sessions.</li>
          <li>Add many new translations.</li>
        </ul>
      </description>
    </release>
    <release version="1.1.0" date="2021-09-22">
      <description>
        <p>The 1.1.0 release of Metronome brings bug fixes and improved metadata.</p>
        <ul>
          <li>Make the timer button's focus ring visible.</li>
          <li>Declare the supported controls and displays.</li>
          <li>Make the application's summary snappier.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.0" date="2021-08-04">
      <description>
        <p>First version of the application, offering the following features:</p>
        <ul>
          <li>Beat the rhythm to 2/4, 3/4, 4/4 or 6/8.</li>
          <li>High and low click sounds.</li>
          <li>Set the beats-per-minute in the 20–260 interval.</li>
          <li>Tap to set the beating interval.</li>
        </ul>
      </description>
    </release>
  </releases>
  <kudos>
    <kudo>HighContrast</kudo>
    <kudo>HiDpiIcon</kudo>
    <kudo>ModernToolkit</kudo>
  </kudos>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <!-- developer_name tag deprecated with Appstream 1.0 -->
  <developer_name translatable="no">Adrien Plazas</developer_name>
  <developer id="com.adrienplazas">
    <name translatable="no">Adrien Plazas</name>
  </developer>
  <update_contact>kekun.plazas@laposte.net</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <custom>
    <value key="Purism::form_factor">mobile</value>
    <value key="GnomeSoftware::key-colors">[(124, 220, 168)]</value>
  </custom>
</component>
